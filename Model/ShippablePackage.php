<?php
/**
 * Created by Rubikin Team.
 * Date: 5/26/14
 * Time: 12:34 AM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentCommonBundle\Model;

use Nilead\ShipmentCommonComponent\Model\ShippablePackageInterface;
use Nilead\ShipmentCommonComponent\Model\ShippingMethodInterface;
use Nilead\ShipmentCommonComponent\Model\Traits\ShippableTrait;
use Nilead\ShipmentComponent\Model\ShipmentAddressInterface;


abstract class ShippablePackage implements ShippablePackageInterface
{
    use ShippableTrait;

    /**
     * @var ShipmentAddressInterface
     */
    protected $shipToAddress;

    /**
     * @var ShipmentAddressInterface
     */
    protected $shipFromAddress;

    /**
     * @var ShippingMethodInterface
     */
    protected $shippingMethod;

    /**
     * {@inheritdoc}
     */
    public function setShipToAddress(ShipmentAddressInterface $address = null)
    {
        $this->shipToAddress = $address;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setShipFromAddress(ShipmentAddressInterface $address)
    {
        $this->shipFromAddress = $address;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getShipFromAddress()
    {
        return $this->shipFromAddress;
    }

    /**
     * {@inheritdoc}
     */
    public function getShipToAddress()
    {
        return $this->shipToAddress;
    }

    /**
     * {@inheritdoc}
     */
    public function setShippingMethod(ShippingMethodInterface $shippingMethod = null)
    {
        $this->shippingMethod = $shippingMethod;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getShippingMethod()
    {
        return $this->shippingMethod;
    }
}
