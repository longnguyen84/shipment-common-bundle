<?php
/**
 * Created by Rubikin Team.
 * ========================
 * Date: 10/20/2014
 * Time: 1:43 AM
 * Author: vu
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentCommonBundle\Model;


use Doctrine\Common\Collections\ArrayCollection;
use Nilead\ShipmentCommonComponent\Model\ShipReadyInterface;
use Nilead\ShipmentCommonComponent\Model\ShipReadyPackageInterface;

class ShipReadyPackage extends ShippablePackage implements ShipReadyPackageInterface
{
    /**
     * @var ArrayCollection|ShipReadyInterface[]
     */
    protected $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function addItem(ShipReadyInterface $shipReady)
    {
        $this->items->add($shipReady);
    }

    /**
     * {@inheritdoc}
     */
    public function setItems(ArrayCollection $items)
    {
        $this->items = $items;
    }

    /**
     * {@inheritdoc}
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * {@inheritdoc}
     */
    public function getQuantity()
    {
        $count = 0;
        foreach ($this->getItems() as $item) {
            /** @var ShipReadyInterface $item */
            $count += $item->getQuantity();
        }

        return $count;
    }
}
